package service

import (
	"context"
	"payservice/grpc/client"
	"payservice/pkg/logger"
	"payservice/storage"
	pbp "payservice/genproto/payment_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type saleProductService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewSaleProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *saleProductService {
	return &saleProductService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *saleProductService) Create(ctx context.Context, request *pbp.CreateSaleProduct) (*pbp.SaleProduct, error) {
	return o.storage.SaleProduct().Create(ctx, request)
}
func (o *saleProductService) Get(ctx context.Context, request *pbp.PrimaryKeySaleProduct) (*pbp.SaleProduct, error) {
	return o.storage.SaleProduct().Get(ctx, request)
}
func (o *saleProductService) GetList(ctx context.Context, request *pbp.SaleProductRequest) (*pbp.SaleProductResponse, error) {
	return o.storage.SaleProduct().GetList(ctx, request)
}
func (o *saleProductService) Update(ctx context.Context, request *pbp.SaleProduct) (*pbp.SaleProduct, error) {
	return o.storage.SaleProduct().Update(ctx, request)
}
func (o *saleProductService) Delete(ctx context.Context, request *pbp.PrimaryKeySaleProduct) (*emptypb.Empty, error) {
	return o.storage.SaleProduct().Delete(ctx, request)
}
