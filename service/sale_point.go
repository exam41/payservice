package service

import (
	"context"
	pbp "payservice/genproto/payment_service"

	"payservice/grpc/client"
	"payservice/pkg/generate"
	"payservice/pkg/logger"
	"payservice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type salePointService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewSalePointService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *salePointService {
	return &salePointService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *salePointService) Create(ctx context.Context, request *pbp.SalesPoint) (*pbp.SalesPoint, error) {

	countedid, err := o.services.SalePointService().Count(context.Background(), &emptypb.Empty{})
	if err != nil {
		request.Id = "S-0001"
	} else {
		request.Id = generate.GenerateNextID(countedid.Id, request.Name)
	}

	return o.storage.SalePoint().Create(ctx, request)
}

func (o *salePointService) Get(ctx context.Context, request *pbp.PrimaryKeySalePoint) (*pbp.SalesPoint, error) {
	return o.storage.SalePoint().Get(ctx, request)
}
func (o *salePointService) GetList(ctx context.Context, request *pbp.SalePointRequest) (*pbp.SalesPointResponse, error) {
	return o.storage.SalePoint().GetList(ctx, request)
}
func (o *salePointService) Update(ctx context.Context, request *pbp.SalesPoint) (*pbp.SalesPoint, error) {
	return o.storage.SalePoint().Update(ctx, request)
}
func (o *salePointService) Delete(ctx context.Context, request *pbp.PrimaryKeySalePoint) (*emptypb.Empty, error) {
	return o.storage.SalePoint().Delete(ctx, request)
}
func (o *salePointService) Count(ctx context.Context, request *emptypb.Empty) (*pbp.PrimaryKeySalePoint, error) {
	return o.storage.SalePoint().Count(ctx, &emptypb.Empty{})
}
