package service

import (
	"context"
	pbp "payservice/genproto/payment_service"
	"payservice/grpc/client"
	"payservice/pkg/logger"
	"payservice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type staffService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewStaffService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *staffService {
	return &staffService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *staffService) Create(ctx context.Context, request *pbp.CreateStaff) (*pbp.Staffs, error) {
	return o.storage.Staff().Create(ctx, request)
}
func (o *staffService) Get(ctx context.Context, request *pbp.PrimaryKeyStaffs) (*pbp.Staffs, error) {
	return o.storage.Staff().Get(ctx, request)
}
func (o *staffService) GetList(ctx context.Context, request *pbp.StaffRequest) (*pbp.StaffResponse, error) {
	return o.storage.Staff().GetList(ctx, request)
}
func (o *staffService) Update(ctx context.Context, request *pbp.Staffs) (*pbp.Staffs, error) {
	return o.storage.Staff().Update(ctx, request)
}
func (o *staffService) Delete(ctx context.Context, request *pbp.PrimaryKeyStaffs) (*emptypb.Empty, error) {
	return o.storage.Staff().Delete(ctx, request)
}
