package service

import (
	"context"
	pbp "payservice/genproto/payment_service"
	"payservice/grpc/client"
	"payservice/pkg/genid"
	"payservice/pkg/logger"
	"payservice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type shiftService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewShiftService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *shiftService {
	return &shiftService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *shiftService) Create(ctx context.Context, request *pbp.Shifts) (*pbp.Shifts, error) {
	countedid, err := o.services.ShiftService().Count(context.Background(), &emptypb.Empty{})
	if err != nil {
		request.Id = "Sh-0001"
	} else {
		request.Id = genid.GenerateID(countedid.Id)
	}

	return o.storage.Shift().Create(ctx, request)
}
func (o *shiftService) Get(ctx context.Context, request *pbp.PrimaryKeyShift) (*pbp.Shifts, error) {
	return o.storage.Shift().Get(ctx, request)
}
func (o *shiftService) GetList(ctx context.Context, request *pbp.ShiftRequest) (*pbp.ShiftResponse, error) {
	return o.storage.Shift().GetList(ctx, request)
}
func (o *shiftService) Update(ctx context.Context, request *pbp.Shifts) (*pbp.Shifts, error) {
	return o.storage.Shift().Update(ctx, request)
}
func (o *shiftService) Delete(ctx context.Context, request *pbp.PrimaryKeyShift) (*emptypb.Empty, error) {
	return o.storage.Shift().Delete(ctx, request)
}

func (o *shiftService) Patch(ctx context.Context, request *pbp.UpdateStatus) (*pbp.UpdateStatus, error) {
	return o.storage.Shift().Patch(ctx, request)
}

func (o *shiftService) Count(ctx context.Context, request *emptypb.Empty) (*pbp.PrimaryKeyShift, error) {
	return o.storage.Shift().Count(ctx, &emptypb.Empty{})
}
