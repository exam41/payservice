package service

import (
	"context"
	"payservice/grpc/client"
	"payservice/pkg/logger"
	"payservice/storage"

	pbp "payservice/genproto/payment_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewBranchService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *branchService {
	return &branchService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (o *branchService) Create(ctx context.Context, request *pbp.Branch) (*pbp.Branch, error) {
	// id, err := o.storage.Branch().Count(context.Background(), &emptypb.Empty{})
	// if err != nil {
	// 	fmt.Println("error while counting branch", err)
	// }
	// idstring := generate.GenerateExternalID(id.Id, request.Name)
	// request.Id = idstring
	return o.storage.Branch().Create(ctx, request)
}
func (o *branchService) Get(ctx context.Context, request *pbp.PrimaryKeyBranch) (*pbp.Branch, error) {
	return o.storage.Branch().Get(ctx, request)
}
func (o *branchService) GetList(ctx context.Context, request *pbp.BranchRequest) (*pbp.BranchResponse, error) {
	return o.storage.Branch().GetList(ctx, request)
}
func (o *branchService) Update(ctx context.Context, request *pbp.Branch) (*pbp.Branch, error) {
	return o.storage.Branch().Update(ctx, request)
}
func (o *branchService) Delete(ctx context.Context, request *pbp.PrimaryKeyBranch) (*emptypb.Empty, error) {
	return o.storage.Branch().Delete(ctx, request)
}
func (o *branchService) Count(ctx context.Context, request *emptypb.Empty) (*pbp.PrimaryKeyBranch, error) {
	return o.storage.Branch().Count(ctx, &emptypb.Empty{})
}
