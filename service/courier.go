package service

import (
	"context"
	pbp "payservice/genproto/payment_service"
	"payservice/grpc/client"
	"payservice/pkg/logger"
	"payservice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCourierService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *courierService {
	return &courierService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (c *courierService) Create(ctx context.Context, request *pbp.CreateCourier) (*pbp.Courier, error) {
	return c.storage.Courier().Create(ctx, request)
}
func (c *courierService) Get(ctx context.Context, request *pbp.PrimaryKeyCourier) (*pbp.Courier, error) {
	return c.storage.Courier().Get(ctx, request)
}
func (o *courierService) GetList(ctx context.Context, request *pbp.CourierRequest) (*pbp.CourierResponse, error) {
	return o.storage.Courier().GetList(ctx, request)
}
func (o *courierService) Update(ctx context.Context, request *pbp.Courier) (*pbp.Courier, error) {
	return o.storage.Courier().Update(ctx, request)
}
func (o *courierService) Delete(ctx context.Context, request *pbp.PrimaryKeyCourier) (*emptypb.Empty, error) {
	return o.storage.Courier().Delete(ctx, request)
}
