package service

import (
	"context"
	"fmt"
	pbp "payservice/genproto/payment_service"
	pbr "payservice/genproto/repo_service"
	"payservice/grpc/client"
	"payservice/pkg/barcode"
	"payservice/pkg/genid"
	"payservice/pkg/logger"
	"payservice/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type saleService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewSaleService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *saleService {
	return &saleService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (o *saleService) Create(ctx context.Context, request *pbp.Sales) (*pbp.Sales, error) {
	// today := time.Now().Format("2006-01-02")
	// fmt.Println("ffffff")
	// count, err := o.storage.Sale().CountQuery(context.Background(), &pbp.Date{Date: today})
	// if err != nil {
	// 	request.Id = time.Now().Format("2006-01-02") + "1"
	// } else {
	// 	fmt.Println("hhhhhhh")
	// 	salenumber := count.Count + 1
	// 	saleID := fmt.Sprintf("%s %d", today, salenumber)
	// 	request.Id = saleID
	// }

	fmt.Println("shift id",request.ShiftId)
	shiftid, err := o.services.ShiftService().Get(context.Background(), &pbp.PrimaryKeyShift{Id: request.ShiftId})
	if err != nil {
		fmt.Println("err is here", err)
	}
	fmt.Println("tttttttttt",shiftid.Status)
	if shiftid.Status != "closed" {
		fmt.Println("close the shift first!")
		return nil, nil
	}
	countid, err := o.services.SaleService().CountSale(context.Background(), &emptypb.Empty{})
	if err != nil {
		request.Id = "S-0001"
	} else {
		request.Id = genid.GenerateID(countid.Id)
	}
	request.Barcode = barcode.BarcodeGenerate()
	return o.storage.Sale().Create(ctx, request)
}

func (o *saleService) Get(ctx context.Context, request *pbp.PrimaryKeySale) (*pbp.Sales, error) {

	return o.storage.Sale().Get(ctx, request)
}
func (o *saleService) GetList(ctx context.Context, request *pbp.SaleRequest) (*pbp.SalesResponse, error) {
	return o.storage.Sale().GetList(ctx, request)
}
func (o *saleService) Update(ctx context.Context, request *pbp.Sales) (*pbp.Sales, error) {
	return o.storage.Sale().Update(ctx, request)
}
func (o *saleService) Delete(ctx context.Context, request *pbp.PrimaryKeySale) (*emptypb.Empty, error) {
	return o.storage.Sale().Delete(ctx, request)
}

func (o *saleService) CountQuery(ctx context.Context, request *pbp.Date) (*pbp.CountQueryy, error) {
	return o.storage.Sale().CountQuery(ctx, request)
}

func (o *saleService) EndSale(ctx context.Context, request *pbp.PrimaryKeySale) (*emptypb.Empty, error) {

	sale, err := o.services.SaleService().Get(context.Background(), &pbp.PrimaryKeySale{Id: request.Id})
	if err != nil {
		fmt.Println("Error", err)
	}
	updatedsale, err := o.services.SaleService().Update(context.Background(), &pbp.Sales{Id: sale.Id, Barcode: sale.Barcode, ShiftId: sale.ShiftId, SalePointId: sale.SalePointId, StaffId: sale.StaffId, Status: "closed"})
	if err != nil {
		fmt.Println("err", err)
	}

	saleproduct, err := o.services.SaleProductService().Get(context.Background(), &pbp.PrimaryKeySaleProduct{Id: updatedsale.Id})
	if err != nil {
		fmt.Println("Err", err)
	}

	storage, err := o.services.Storage().GetList(context.Background(), &pbr.StorageRequest{Page: 1, Limit: 10})
	if err != nil {
		fmt.Println("Err", err)

	}

	for _, v := range storage.Storages {
		if saleproduct.SaleId == v.SalePointId {
			updatedstorage, err := o.services.Storage().Update(context.Background(), &pbr.Storage{Id: v.Id, SalePointId: v.SalePointId, Name: v.Name, IncomePrice: v.IncomePrice, Barcode: v.Barcode, TotalPrice: v.TotalPrice, ProductId: v.ProductId, Quantity: v.Quantity - saleproduct.Quantity})
			if err != nil {
				fmt.Println("Err")
			}
			fmt.Println("updatedstorage", updatedstorage.Id)

		}
	}

	return nil, nil
}

func (o *saleService) CountSale(ctx context.Context, request *emptypb.Empty) (*pbp.PrimaryKeySale, error) {
	return o.services.SaleService().CountSale(ctx, request)
}
