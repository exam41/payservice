// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: shifts_service.proto

package payment_service

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ShiftsServiceClient is the client API for ShiftsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ShiftsServiceClient interface {
	Create(ctx context.Context, in *Shifts, opts ...grpc.CallOption) (*Shifts, error)
	Get(ctx context.Context, in *PrimaryKeyShift, opts ...grpc.CallOption) (*Shifts, error)
	GetList(ctx context.Context, in *ShiftRequest, opts ...grpc.CallOption) (*ShiftResponse, error)
	Update(ctx context.Context, in *Shifts, opts ...grpc.CallOption) (*Shifts, error)
	Delete(ctx context.Context, in *PrimaryKeyShift, opts ...grpc.CallOption) (*empty.Empty, error)
	Patch(ctx context.Context, in *UpdateStatus, opts ...grpc.CallOption) (*UpdateStatus, error)
	Count(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*PrimaryKeyShift, error)
}

type shiftsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewShiftsServiceClient(cc grpc.ClientConnInterface) ShiftsServiceClient {
	return &shiftsServiceClient{cc}
}

func (c *shiftsServiceClient) Create(ctx context.Context, in *Shifts, opts ...grpc.CallOption) (*Shifts, error) {
	out := new(Shifts)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) Get(ctx context.Context, in *PrimaryKeyShift, opts ...grpc.CallOption) (*Shifts, error) {
	out := new(Shifts)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Get", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) GetList(ctx context.Context, in *ShiftRequest, opts ...grpc.CallOption) (*ShiftResponse, error) {
	out := new(ShiftResponse)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) Update(ctx context.Context, in *Shifts, opts ...grpc.CallOption) (*Shifts, error) {
	out := new(Shifts)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) Delete(ctx context.Context, in *PrimaryKeyShift, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) Patch(ctx context.Context, in *UpdateStatus, opts ...grpc.CallOption) (*UpdateStatus, error) {
	out := new(UpdateStatus)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Patch", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *shiftsServiceClient) Count(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*PrimaryKeyShift, error) {
	out := new(PrimaryKeyShift)
	err := c.cc.Invoke(ctx, "/repo_Service.ShiftsService/Count", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ShiftsServiceServer is the server API for ShiftsService service.
// All implementations should embed UnimplementedShiftsServiceServer
// for forward compatibility
type ShiftsServiceServer interface {
	Create(context.Context, *Shifts) (*Shifts, error)
	Get(context.Context, *PrimaryKeyShift) (*Shifts, error)
	GetList(context.Context, *ShiftRequest) (*ShiftResponse, error)
	Update(context.Context, *Shifts) (*Shifts, error)
	Delete(context.Context, *PrimaryKeyShift) (*empty.Empty, error)
	Patch(context.Context, *UpdateStatus) (*UpdateStatus, error)
	Count(context.Context, *empty.Empty) (*PrimaryKeyShift, error)
}

// UnimplementedShiftsServiceServer should be embedded to have forward compatible implementations.
type UnimplementedShiftsServiceServer struct {
}

func (UnimplementedShiftsServiceServer) Create(context.Context, *Shifts) (*Shifts, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedShiftsServiceServer) Get(context.Context, *PrimaryKeyShift) (*Shifts, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Get not implemented")
}
func (UnimplementedShiftsServiceServer) GetList(context.Context, *ShiftRequest) (*ShiftResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedShiftsServiceServer) Update(context.Context, *Shifts) (*Shifts, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedShiftsServiceServer) Delete(context.Context, *PrimaryKeyShift) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedShiftsServiceServer) Patch(context.Context, *UpdateStatus) (*UpdateStatus, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Patch not implemented")
}
func (UnimplementedShiftsServiceServer) Count(context.Context, *empty.Empty) (*PrimaryKeyShift, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Count not implemented")
}

// UnsafeShiftsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ShiftsServiceServer will
// result in compilation errors.
type UnsafeShiftsServiceServer interface {
	mustEmbedUnimplementedShiftsServiceServer()
}

func RegisterShiftsServiceServer(s grpc.ServiceRegistrar, srv ShiftsServiceServer) {
	s.RegisterService(&ShiftsService_ServiceDesc, srv)
}

func _ShiftsService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Shifts)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Create(ctx, req.(*Shifts))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_Get_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PrimaryKeyShift)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Get(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Get",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Get(ctx, req.(*PrimaryKeyShift))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ShiftRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).GetList(ctx, req.(*ShiftRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Shifts)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Update(ctx, req.(*Shifts))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PrimaryKeyShift)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Delete(ctx, req.(*PrimaryKeyShift))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_Patch_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateStatus)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Patch(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Patch",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Patch(ctx, req.(*UpdateStatus))
	}
	return interceptor(ctx, in, info, handler)
}

func _ShiftsService_Count_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ShiftsServiceServer).Count(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/repo_Service.ShiftsService/Count",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ShiftsServiceServer).Count(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

// ShiftsService_ServiceDesc is the grpc.ServiceDesc for ShiftsService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ShiftsService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "repo_Service.ShiftsService",
	HandlerType: (*ShiftsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _ShiftsService_Create_Handler,
		},
		{
			MethodName: "Get",
			Handler:    _ShiftsService_Get_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _ShiftsService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _ShiftsService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _ShiftsService_Delete_Handler,
		},
		{
			MethodName: "Patch",
			Handler:    _ShiftsService_Patch_Handler,
		},
		{
			MethodName: "Count",
			Handler:    _ShiftsService_Count_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "shifts_service.proto",
}
