drop type if exists user_role_enum;
drop type if exists status_shift_enum;
drop type if exists payment_type_enum;

drop table if exists "sale_products";
drop table if exists "sales";
drop table if exists "shifts";
drop table if exists "couriers";
drop table if exists "staffs";
drop table if exists "sale_points";
drop table if exists "branches";
