
CREATE TYPE  "user_role_enum" AS ENUM (
  'shop_assistant',
  'admin',
  'repo_admin'
);

CREATE TYPE  "status_shift_enum" AS ENUM (
  'opened',
  'closed'
);

CREATE TYPE  "payment_type_enum" AS ENUM (
  'cash',
  'card'
);


CREATE TABLE  "branches" (
  "id" uuid PRIMARY KEY,
  "name" varchar(100),
  "address" varchar(100),
  "phone" varchar(13),
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);

-- Create "sales_points" table
CREATE TABLE "sales_points" (
  "id" varchar(50) PRIMARY KEY,
  "branch_id" uuid REFERENCES branches(id), 
  "name" varchar(100),
  "income_id" varchar(10),
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);


CREATE TABLE  "staffs" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar(100),
  "last_name" varchar(100),
  "phone" varchar(13),
  "login" varchar UNIQUE,
  "password" text,
  "sales_point_id" varchar(100),
  "user_role" user_role_enum,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);


CREATE TABLE  "couriers" (
  "id" uuid PRIMARY KEY,
  "name" varchar(100),
  "phone" varchar(13),
  "active" bool DEFAULT true,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);

-- Create "shifts" table
CREATE TABLE  "shifts" (
  "id" varchar(10) PRIMARY KEY,
  "staff_id" uuid,
  "sale_point_id" varchar(10),
  "start_hour" timestamp,
  "end_hour" timestamp,
  "status" status_shift_enum,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);

CREATE TABLE  "sales" (
  "id" varchar(20) UNIQUE,
  "barcode" varchar(10) UNIQUE,
  "shift_id" varchar(10),
  "sale_point_id" varchar(10),
  "staff_id" uuid,
  "status" status_shift_enum DEFAULT 'opened',
  "searching_column" varchar(100),
  "created_at" timestamp,
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);

CREATE TABLE  "sale_products" (
  "id" uuid,
  "sale_id" varchar(20) REFERENCES sales(id),
  "product_id" uuid,
  "barcode" varchar(10) UNIQUE,
  "quantity" int,
  "exist_product_quantity" int,
  "price" int,
  "total_price" int,
  "payment" payment_type_enum,
  "searching_column" varchar(100),
  "created_at" timestamp DEFAULT now(),
  "updated_at" timestamp DEFAULT now(),
  "deleted_at" int DEFAULT 0
);

ALTER TABLE "staffs" ADD FOREIGN KEY ("sales_point_id") REFERENCES "sales_points" ("id");

ALTER TABLE "shifts" ADD FOREIGN KEY ("staff_id") REFERENCES "staffs" ("id");

ALTER TABLE "shifts" ADD FOREIGN KEY ("sale_point_id") REFERENCES "sales_points" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("shift_id") REFERENCES "shifts" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("sale_point_id") REFERENCES "sales_points" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("staff_id") REFERENCES "staffs" ("id");

ALTER TABLE "sale_products" ADD FOREIGN KEY ("sale_id") REFERENCES "sales" ("id");
