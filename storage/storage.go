package storage

import (
	"context"

	pbp "payservice/genproto/payment_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	Courier() ICourierStorage
	Sale() ISalesStorage
	SalePoint() ISalesPointStorage
	SaleProduct() ISaleProductsStorage
	Shift() IShiftsStorage
	Staff() IStaffsStorage
}

type IBranchStorage interface {
	Create(context.Context, *pbp.Branch) (*pbp.Branch, error)
	Get(context.Context, *pbp.PrimaryKeyBranch) (*pbp.Branch, error)
	GetList(context.Context, *pbp.BranchRequest) (*pbp.BranchResponse, error)
	Update(context.Context, *pbp.Branch) (*pbp.Branch, error)
	Delete(context.Context, *pbp.PrimaryKeyBranch) (*emptypb.Empty, error)
	Count(context.Context,*emptypb.Empty)(*pbp.PrimaryKeyBranch,error)
}
type ICourierStorage interface {
	Create(context.Context, *pbp.CreateCourier) (*pbp.Courier, error)
	Get(context.Context, *pbp.PrimaryKeyCourier) (*pbp.Courier, error)
	GetList(context.Context, *pbp.CourierRequest) (*pbp.CourierResponse, error)
	Update(context.Context, *pbp.Courier) (*pbp.Courier, error)
	Delete(context.Context, *pbp.PrimaryKeyCourier) (*emptypb.Empty, error)
}

type ISalesPointStorage interface {
	Create(context.Context, *pbp.SalesPoint) (*pbp.SalesPoint, error)
	Get(context.Context, *pbp.PrimaryKeySalePoint) (*pbp.SalesPoint, error)
	GetList(context.Context, *pbp.SalePointRequest) (*pbp.SalesPointResponse, error)
	Update(context.Context, *pbp.SalesPoint) (*pbp.SalesPoint, error)
	Delete(context.Context, *pbp.PrimaryKeySalePoint) (*emptypb.Empty, error)
	Count(context.Context,*emptypb.Empty)(*pbp.PrimaryKeySalePoint,error)
}

type ISaleProductsStorage interface {
	Create(context.Context, *pbp.CreateSaleProduct) (*pbp.SaleProduct, error)
	Get(context.Context, *pbp.PrimaryKeySaleProduct) (*pbp.SaleProduct, error)
	GetList(context.Context, *pbp.SaleProductRequest) (*pbp.SaleProductResponse, error)
	Update(context.Context, *pbp.SaleProduct) (*pbp.SaleProduct, error)
	Delete(context.Context, *pbp.PrimaryKeySaleProduct) (*emptypb.Empty, error)
}

type ISalesStorage interface {
	Create(context.Context, *pbp.Sales) (*pbp.Sales, error)
	Get(context.Context, *pbp.PrimaryKeySale) (*pbp.Sales, error)
	GetList(context.Context, *pbp.SaleRequest) (*pbp.SalesResponse, error)
	Update(context.Context, *pbp.Sales) (*pbp.Sales, error)
	Delete(context.Context, *pbp.PrimaryKeySale) (*emptypb.Empty, error)
	CountQuery(context.Context,*pbp.Date)(*pbp.CountQueryy,error)
	CountSale(context.Context,*emptypb.Empty)(*pbp.PrimaryKeySale,error)
}

type IShiftsStorage interface {
	Create(context.Context, *pbp.Shifts) (*pbp.Shifts, error)
	Get(context.Context, *pbp.PrimaryKeyShift) (*pbp.Shifts, error)
	GetList(context.Context, *pbp.ShiftRequest) (*pbp.ShiftResponse, error)
	Update(context.Context, *pbp.Shifts) (*pbp.Shifts, error)
	Delete(context.Context, *pbp.PrimaryKeyShift) (*emptypb.Empty, error)
	Patch(context.Context, *pbp.UpdateStatus) (*pbp.UpdateStatus, error)
	Count(context.Context,*emptypb.Empty)(*pbp.PrimaryKeyShift,error)
}


type IStaffsStorage interface {
	Create(context.Context, *pbp.CreateStaff) (*pbp.Staffs, error)
	Get(context.Context, *pbp.PrimaryKeyStaffs) (*pbp.Staffs, error)
	GetList(context.Context, *pbp.StaffRequest) (*pbp.StaffResponse, error)
	Update(context.Context, *pbp.Staffs) (*pbp.Staffs, error)
	Delete(context.Context, *pbp.PrimaryKeyStaffs) (*emptypb.Empty, error)
}
