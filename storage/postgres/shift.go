package postgres

import (
	"context"
	"errors"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"

	pbp "payservice/genproto/payment_service"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type shiftRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewShiftRepo(db *pgxpool.Pool, log logger.ILogger) storage.IShiftsStorage {
	return &shiftRepo{
		db:  db,
		log: log,
	}
}
func (s *shiftRepo) Create(ctx context.Context, request *pbp.Shifts) (*pbp.Shifts, error) {

	shift := pbp.Shifts{}

	query := ` insert into shifts (id,staff_id,sale_point_id,start_hour,end_hour,status) values($1,$2,$3,to_timestamp($4, 'HH24:MI:SS'),to_timestamp($5, 'HH24:MI:SS'),$6)
	        returning id, staff_id, sale_point_id,start_hour::varchar, end_hour::varchar,status
	 `

	//searchingColumn := fmt.Sprintf("%s %s %s", request.GetStaffId(), request.GetSalePointId(), request.GetStatus())
	if err := s.db.QueryRow(ctx, query, request.Id, request.GetStaffId(), request.GetSalePointId(), request.GetStartHour(), request.GetEndHour(), request.GetStatus()).Scan(
		&shift.Id,
		&shift.StaffId,
		&shift.SalePointId,
		&shift.StartHour,
		&shift.EndHour,
		&shift.Status,
	); err != nil {
		//s.log.Error("error while creating shift in postgres", logger.Error(err))
		fmt.Println("err", err)

	}
	return &shift, nil
}

func (s *shiftRepo) Get(ctx context.Context, request *pbp.PrimaryKeyShift) (*pbp.Shifts, error) {

	shift := pbp.Shifts{}
	query := ` select id,staff_id, sale_point_id,start_hour::varchar, end_hour::varchar,status from shifts where deleted_at=0 and id=$1 `
	if err := s.db.QueryRow(ctx, query, request.GetId()).Scan(
		&shift.Id,
		&shift.StaffId,
		&shift.SalePointId,
		&shift.StartHour,
		&shift.EndHour,
		&shift.Status,
	); err != nil {
		//s.log.Error("error while getting shift in postgres", logger.Error(err))
		fmt.Println("Err",err)

	}
	return &shift, nil
}

func (s *shiftRepo) GetList(ctx context.Context, request *pbp.ShiftRequest) (*pbp.ShiftResponse, error) {

	var (
		resp   = pbp.ShiftResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from shifts ` + filter

	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error counting shifts", logger.Error(err))
	}
	query := ` select id,staff_id, sale_point_id,start_hour::varchar, end_hour::varchar,status from shifts ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := s.db.Query(ctx, query)
	if err != nil {
		s.log.Error("error while getting shifts in postgres", logger.Error(err))

	}
	defer rows.Close()
	for rows.Next() {
		shift := pbp.Shifts{}
		if err := rows.Scan(
			&shift.Id,
			&shift.StaffId,
			&shift.SalePointId,
			&shift.StartHour,
			&shift.EndHour,
			&shift.Status,
		); err != nil {
			s.log.Error("error while getting shifts in postgres", logger.Error(err))

		}
		resp.Shifts = append(resp.Shifts, &shift)
	}
	resp.Count = count
	return &resp, nil
}

func (s *shiftRepo) Update(ctx context.Context, request *pbp.Shifts) (*pbp.Shifts, error) {

	var (
		shift  = pbp.Shifts{}
		params = make(map[string]interface{})
		query  = ` update shifts set  `
		filter = ""
	)
	params["id"] = request.GetId()
	fmt.Println("shift id", request.GetId())
	if request.GetStaffId() != "" {
		params["staff_id"] = request.StaffId
		filter += " staff_id= @staff_id, "
	}

	if request.GetSalePointId() != "" {
		params["sale_point_id"] = request.SalePointId
		filter += " sale_point_id= @sale_point_id, "
	}
	if request.GetStartHour() != "" {
		params["start_hour"] = request.StartHour
		filter += " start_hour= @start_hour, "
	}
	if request.GetEndHour() != "" {
		params["end_hour"] = request.EndHour
		filter += " end_hour= @end_hour, "
	}
	if request.GetStatus() != "" {
		params["status"] = request.Status
		filter += " status= @status, "
	}
	query += filter + `  updated_at=now() where deleted_at=0 and id= @id returning id, staff_id, sale_point_id,start_hour::varchar, end_hour::varchar,status `

	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&shift.Id,
		&shift.BranchId,
		&shift.StaffId,
		&shift.SalePointId,
		&shift.StartHour,
		&shift.EndHour,
		&shift.Status,
	); err != nil {
		s.log.Error("error while updating shift in postgres", logger.Error(err))
	}
	return &shift, nil

}

func (s *shiftRepo) Delete(ctx context.Context, request *pbp.PrimaryKeyShift) (*emptypb.Empty, error) {

	query := ` update shifts set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := s.db.Exec(ctx, query, request.GetId())
	return nil, err
}

func (s *shiftRepo) Patch(ctx context.Context, request *pbp.UpdateStatus) (*pbp.UpdateStatus, error) {

	updatedstatus:=pbp.UpdateStatus{}
	query := ` update shifts set status=$1 where id = $2`
	_, err := s.db.Exec(ctx, query, request.GetStatus(), request.GetShiftId())
	if err != nil {
		//s.log.Error("error while updating shift in postgres", logger.Error(err))
		fmt.Println("err", err)
	}
	updatedstatus.ShiftId=request.GetShiftId()
	updatedstatus.Status=request.GetStatus()
	return &updatedstatus, nil
}

func (s *shiftRepo) Count(ctx context.Context, empty *emptypb.Empty) (*pbp.PrimaryKeyShift, error) {

	count := pbp.PrimaryKeyShift{}

	query := `SELECT id FROM shifts ORDER BY id DESC LIMIT 1`

	row := s.db.QueryRow(ctx, query)
	var id string
	err := row.Scan(&id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			id = "Sh-0001" 
		} else {
			return nil, err
		}
	}
	count.Id = id
	return &count, nil
}
