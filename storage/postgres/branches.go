package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"

	pbp "payservice/genproto/payment_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchStorage {
	return &branchRepo{
		db:  db,
		log: log,
	}
}

func (u *branchRepo) Create(ctx context.Context, request *pbp.Branch) (*pbp.Branch, error) {

	branch := pbp.Branch{}

	query := `insert into branches(id, name, address, phone, searching_column) values ($1, $2, $3, $4, $5) returning id, name, address, phone `

	uid:=uuid.New().String()
	searchingColumn := fmt.Sprintf("%s %s %s", request.GetName(), request.GetAddress(), request.GetPhone())
	if err := u.db.QueryRow(ctx, query,uid, request.GetName(), request.GetAddress(), request.GetPhone(), searchingColumn).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Address,
		&branch.Phone,
	); err != nil {
		fmt.Println("err",err)
	}
	return &branch, nil
}
func (u *branchRepo) Get(ctx context.Context, request *pbp.PrimaryKeyBranch) (*pbp.Branch, error) {
	branch := pbp.Branch{}

	var phonestr, namestr, addresstr sql.NullString

	query := ` select id, name, address, phone from branches where deleted_at=0 and id=$1 `
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&branch.Id,
		&namestr,
		&addresstr,
		&phonestr,
	); err != nil {
		u.log.Error("error while getting branch in postgres ", logger.Error(err))
	}
	if addresstr.Valid {
		branch.Address = addresstr.String
	}
	if namestr.Valid {
		branch.Name = namestr.String
	}
	if phonestr.Valid {
		branch.Phone = phonestr.String
	}

	return &branch, nil
}
func (u *branchRepo) GetList(ctx context.Context, request *pbp.BranchRequest) (*pbp.BranchResponse, error) {
	var (
		resp   = pbp.BranchResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := `select count(*) from branches` + filter

	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("Error querying branches", logger.Error(err))
	}

	query := `select id,name,address, phone from branches ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("Error getting list branches in postgres ", logger.Error(err))
	}

	defer rows.Close()

	var namestr, addressstr, phonestr sql.NullString
	for rows.Next() {
		branch := pbp.Branch{}

		if err = rows.Scan(
			&branch.Id,
			&namestr,
			&addressstr,
			&phonestr,
		); err != nil {
			u.log.Error("error getting list of branch in postgres ", logger.Error(err))

		}
		if addressstr.Valid {
			branch.Address = addressstr.String
		}
		if namestr.Valid {
			branch.Name = namestr.String
		}
		if phonestr.Valid {
			branch.Phone = phonestr.String
		}
		resp.Branches = append(resp.Branches, &branch)
	}
	resp.Count = count

	return &resp, nil
}
func (u *branchRepo) Update(ctx context.Context, request *pbp.Branch) (*pbp.Branch, error) {
	var (
		branch = pbp.Branch{}
		params = make(map[string]interface{})
		query  = ` update branches set  `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("branch id", request.GetId())

	if request.GetAddress() != "" {
		params["address"] = request.Address
		filter += " address= @address, "
	}
	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name, "
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += "phone= @phone, "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, address, phone  `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.Address,
	); err != nil {
		fmt.Println("error updating branch",err)
	}

	return &branch, nil

}

func (u *branchRepo) Delete(ctx context.Context, request *pbp.PrimaryKeyBranch) (*emptypb.Empty, error) {
	query := `update branches set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)

	return nil, err
}

func (u *branchRepo) Count(ctx context.Context,empty *emptypb.Empty)(*pbp.PrimaryKeyBranch,error)  {
	
	count := pbp.PrimaryKeyBranch{}

	query := `SELECT id FROM branches ORDER BY id DESC LIMIT 1`

	row := u.db.QueryRow(ctx, query)
	var id string
	err := row.Scan(&id)
	if err != nil {
		fmt.Println("err",err)
	
	}
	count.Id = id
	return &count, nil
}