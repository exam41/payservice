package postgres

import (
	"context"
	"errors"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"
	"reflect"

	pbp "payservice/genproto/payment_service"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type salesPointsRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSalesPointsRepo(db *pgxpool.Pool, log logger.ILogger) storage.ISalesPointStorage {
	return &salesPointsRepo{
		db:  db,
		log: log,
	}
}

func (u *salesPointsRepo) Create(ctx context.Context, request *pbp.SalesPoint) (*pbp.SalesPoint, error) {
	salesPoint := pbp.SalesPoint{}

	query := `INSERT INTO sales_points(id,branch_id, name, income_id, searching_column) values ($1,$2,$3,$4,$5) 
	     returning id,branch_id,name,income_id  `

	searchingColumn := fmt.Sprintf("%s %s %s ", request.GetName(), request.GetBranchId(), request.GetIncomeId())

	fmt.Println("www", reflect.TypeOf(request.Id))
	fmt.Println("ssssssssss", request.GetName(), "eeeeee", request.Id, "fffff", request.IncomeId)
	if err := u.db.QueryRow(ctx, query, request.Id, request.GetBranchId(), request.GetName(), request.GetIncomeId(), searchingColumn).Scan(
		&salesPoint.Id,
		&salesPoint.BranchId,
		&salesPoint.Name,
		&salesPoint.IncomeId,
	); err != nil {
		fmt.Println("here error", err.Error())

		fmt.Println("ii", reflect.TypeOf(request.Id))
	}
	fmt.Println("Sale point id", request.Id)
	return &salesPoint, nil

}

func (u *salesPointsRepo) Get(ctx context.Context, request *pbp.PrimaryKeySalePoint) (*pbp.SalesPoint, error) {
	salesPoint := pbp.SalesPoint{}

	query := ` select id, branch_id, name, income_id from sales_points where deleted_at=0 and id=$1 `

	if err := u.db.QueryRow(ctx, query, request.Id).Scan(
		&salesPoint.Id,
		&salesPoint.BranchId,
		&salesPoint.Name,
		&salesPoint.IncomeId,
	); err != nil {
		u.log.Error("error while creating sales point in postgres", logger.Error(err))
	}

	return &salesPoint, nil

}
func (u *salesPointsRepo) GetList(ctx context.Context, request *pbp.SalePointRequest) (*pbp.SalesPointResponse, error) {

	var (
		resp   = pbp.SalesPointResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := ` select count(*) from sales_points ` + filter

	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("Error counting sales points", logger.Error(err))
	}

	query := ` select id, branch_id, name, income_id from sales_points ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)

	if err != nil {
		u.log.Error("error while getting sales points in postgres ", logger.Error(err))
	}

	defer rows.Close()

	for rows.Next() {
		salesPoint := pbp.SalesPoint{}
		if err := rows.Scan(&salesPoint.Id, &salesPoint.BranchId, &salesPoint.Name, &salesPoint.IncomeId); err != nil {
			u.log.Error("error while getting sales points in postgres ", logger.Error(err))
		}
		resp.SalesPoints = append(resp.SalesPoints, &salesPoint)
	}
	resp.Count = count
	return &resp, nil
}

func (u *salesPointsRepo) Update(ctx context.Context, request *pbp.SalesPoint) (*pbp.SalesPoint, error) {

	var (
		salesPoint = pbp.SalesPoint{}
		params     = make(map[string]interface{})
		query      = ` update sales_points set  `
		filter     = ""
	)

	params["id"] = request.GetId()
	fmt.Println("sales point id", request.GetId())

	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name, "
	}

	if request.GetIncomeId() != "" {
		params["income_id"] = request.GetIncomeId()
		filter += " income_id= @income_id, "
	}

	if request.GetBranchId() != "" {
		params["branch_id"] = request.GetBranchId()
		filter += " branch_id= @branch_id, "
	}
	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, income_id, branch_id `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&salesPoint.Id,
		&salesPoint.Name,
		&salesPoint.IncomeId,
		&salesPoint.BranchId,
	); err != nil {
		u.log.Error("error updating sales point in postgres", logger.Error(err))
	}

	return &salesPoint, nil
}

func (u *salesPointsRepo) Delete(ctx context.Context, request *pbp.PrimaryKeySalePoint) (*emptypb.Empty, error) {

	query := ` update sales_points set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, request.Id)
	return nil, err
}

func (u *salesPointsRepo) Count(ctx context.Context, empty *emptypb.Empty) (*pbp.PrimaryKeySalePoint, error) {

	count := pbp.PrimaryKeySalePoint{}

	query := `SELECT id FROM sales_points ORDER BY id DESC LIMIT 1`

	var id string
	err := u.db.QueryRow(ctx, query).Scan(&id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			id = "S-0001"
		} else {
			return nil, err
		}
	}
	count.Id = id
	return &count, nil
}
