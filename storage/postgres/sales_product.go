package postgres

import (
	"context"
	"fmt"

	pbp "payservice/genproto/payment_service"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type salesProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSalesProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.ISaleProductsStorage {
	return &salesProductRepo{
		db:  db,
		log: log,
	}
}

func (s *salesProductRepo) Create(ctx context.Context, request *pbp.CreateSaleProduct) (*pbp.SaleProduct, error) {
	salesProduct := pbp.SaleProduct{}

	query := `INSERT INTO sale_products(id,sale_id, product_id, barcode,quantity,exist_product_quantity,price,total_price,payment,searching_column) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)
	returning id,sale_id, product_id, barcode,quantity,exist_product_quantity,price,total_price,payment`

	searchingColumn := fmt.Sprintf("%s %s %s %d %d %d %d %s ", request.GetSaleId(), request.GetProductId(), request.GetBarcode(), request.GetQuantity(), request.GetExistProductQuantity(), request.GetPrice(), request.GetTotalPrice(), request.GetPaymentType())

	if err := s.db.QueryRow(ctx, query, uuid.New().String(), request.GetSaleId(), request.GetProductId(), request.GetBarcode(), request.GetQuantity(), request.GetExistProductQuantity(), request.GetPrice(), request.GetTotalPrice(), request.GetPaymentType(), searchingColumn).Scan(
		&salesProduct.Id,
		&salesProduct.SaleId,
		&salesProduct.ProductId,
		&salesProduct.Barcode,
		&salesProduct.Quantity,
		&salesProduct.ExistProductQuantity,
		&salesProduct.Price,
		&salesProduct.TotalPrice,
	); err != nil {
		s.log.Error("error while creating sales product in postgres", logger.Error(err))
	}

	return &salesProduct, nil
}

func (s *salesProductRepo) Get(ctx context.Context, request *pbp.PrimaryKeySaleProduct) (*pbp.SaleProduct, error) {
	salesProduct := pbp.SaleProduct{}

	query := `select id,sale_id, product_id, barcode,quantity,exist_product_quantity,price,total_price,payment from sale_products where deleted_at=0 and id=$1`

	if err := s.db.QueryRow(ctx, query, request.Id).Scan(
		&salesProduct.Id,
		&salesProduct.SaleId,
		&salesProduct.ProductId,
		&salesProduct.Barcode,
		&salesProduct.Quantity,
		&salesProduct.ExistProductQuantity,
		&salesProduct.Price,
		&salesProduct.TotalPrice,
	); err != nil {
		s.log.Error("error while getting sales product in postgres", logger.Error(err))
	}

	return &salesProduct, nil
}

func (s *salesProductRepo) GetList(ctx context.Context, request *pbp.SaleProductRequest) (*pbp.SaleProductResponse, error) {
	var (
		resp   = pbp.SaleProductResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := ` select count(*) from sale_products ` + filter

	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error counting sale products", logger.Error(err))
	}

	query := ` select id,sale_id, product_id, barcode,quantity,exist_product_quantity,price,total_price,payment from sale_products ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := s.db.Query(ctx, query)
	if err != nil {
		s.log.Error("error while getting sale products in postgres", logger.Error(err))
	}

	defer rows.Close()
	for rows.Next() {
		salesProduct := pbp.SaleProduct{}
		if err := rows.Scan(
			&salesProduct.Id,
			&salesProduct.SaleId,
			&salesProduct.ProductId,
			&salesProduct.Barcode,
			&salesProduct.Quantity,
			&salesProduct.ExistProductQuantity,
			&salesProduct.Price,
			&salesProduct.TotalPrice,
		); err != nil {
			s.log.Error("error while getting sale products in postgres", logger.Error(err))
		}
		resp.Products = append(resp.Products, &salesProduct)
	}
	resp.Count = count
	return &resp, nil
}

func (s *salesProductRepo) Update(ctx context.Context, request *pbp.SaleProduct) (*pbp.SaleProduct, error) {
	var (
		salesProduct = pbp.SaleProduct{}
		params       = make(map[string]interface{})
		query        = ` update sale_products set  `
		filter       = ""
	)
	params["id"] = request.GetId()
	fmt.Println("sale product id", request.GetId())
	if request.GetSaleId() != "" {
		params["sale_id"] = request.GetSaleId()
		filter += " sale_id= @sale_id, "
	}
	if request.GetProductId() != "" {
		params["product_id"] = request.GetProductId()
		filter += " product_id= @product_id, "
	}
	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()
		filter += " barcode= @barcode, "
	}
	if request.GetQuantity() != 0 {
		params["quantity"] = request.GetQuantity()
		filter += " quantity= @quantity, "
	}
	if request.GetExistProductQuantity() != 0 {
		params["exist_product_quantity"] = request.GetExistProductQuantity()
		filter += " exist_product_quantity= @exist_product_quantity, "
	}
	if request.GetPrice() != 0 {
		params["price"] = request.GetPrice()
		filter += " price= @price, "
	}
	if request.GetTotalPrice() != 0 {
		params["total_price"] = request.GetTotalPrice()
		filter += " total_price= @total_price, "
	}
	if request.GetPaymentType() != "" {
		params["payment"] = request.GetPaymentType()
		filter += " payment= @payment, "
	}
	query+=filter+` updated_at=now() where deleted_at=0 and id=@id returning id,sale_id, product_id, barcode,quantity,exist_product_quantity,price,total_price,payment `
	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&salesProduct.Id,
		&salesProduct.SaleId,
		&salesProduct.ProductId,
		&salesProduct.Barcode,
		&salesProduct.Quantity,
		&salesProduct.ExistProductQuantity,
		&salesProduct.Price,
		&salesProduct.TotalPrice,
	); err != nil {
		s.log.Error("error updating sale product in postgres", logger.Error(err))

	}
	return &salesProduct, nil
}

func (s *salesProductRepo) Delete(ctx context.Context, request *pbp.PrimaryKeySaleProduct) (*emptypb.Empty, error) {
	query := ` update sale_products set deleted_at=extract(epoch from current_timestamp) where id=$1`

	if _, err := s.db.Exec(ctx, query, request.Id); err != nil {
		s.log.Error("error deleting sale product in postgres", logger.Error(err))
	}
	return nil, nil
}
