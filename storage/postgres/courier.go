package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"

	pbp "payservice/genproto/payment_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICourierStorage {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (u *courierRepo) Create(ctx context.Context, request *pbp.CreateCourier) (*pbp.Courier, error) {
	courier := pbp.Courier{}

	uuid:=uuid.New().String()
	query := ` INSERT INTO couriers (id, name, phone, active, searching_column) values ($1,$2,$3,$4,$5) 
           returning id, name, phone, active `
	searchingColumn := fmt.Sprintf("%s %s ", request.GetName(), request.GetPhone())


	if err := u.db.QueryRow(ctx, query, uuid, request.GetName(), request.GetPhone(), request.GetActive(), searchingColumn).Scan(
		&courier.Id,
		&courier.Name,
		&courier.Phone,
		&courier.Active,
	); err != nil {
		//u.log.Error("error while creating courier in postgres", logger.Error(err))
		fmt.Println("Err",err)
	}

	return &courier, nil
}

func (u *courierRepo) Get(ctx context.Context, request *pbp.PrimaryKeyCourier) (*pbp.Courier, error) {

	courier := pbp.Courier{}
	query := ` select id, name, phone, active from couriers where deleted_at=0 and id=$1 `

	var namestr, phonestr sql.NullString
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&courier.Id,
		&courier.Name,
		&courier.Phone,
		&courier.Active,
	); err != nil {
		u.log.Error("error while getting courier in postgres ", logger.Error(err))
	}
	if namestr.Valid {
		courier.Name = namestr.String
	}
	if phonestr.Valid {
		courier.Phone = phonestr.String
	}
	return &courier, nil
}

func (u *courierRepo) GetList(ctx context.Context, request *pbp.CourierRequest) (*pbp.CourierResponse, error) {

	var (
		resp   = pbp.CourierResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := ` select count(*) from couriers ` + filter

	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("Error counting couriers", logger.Error(err))
	}

	query := ` select id , name ,phone, active from couriers ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)

	if err != nil {
		u.log.Error("error while getting courier in postgres ", logger.Error(err))
	}

	defer rows.Close()

	for rows.Next() {
		courier := pbp.Courier{}
		if err := rows.Scan(&courier.Id, &courier.Name, &courier.Phone, &courier.Active); err != nil {
			u.log.Error("error while getting courier in postgres ", logger.Error(err))
		}
		resp.Couriers = append(resp.Couriers, &courier)
	}
	resp.Count = count
	return &resp, nil
}

func (u *courierRepo) Update(ctx context.Context, request *pbp.Courier) (*pbp.Courier, error) {
	var (
		courier = pbp.Courier{}
		params  = make(map[string]interface{})
		query   = ` update couriers set  `
		filter  = ""
	)
	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())
	if request.GetName() != "" {
		params["name"] = request.GetName()
		filter += " name= @name, "
	}
	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()
		filter += " phone= @phone, "
	}

	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active= @active, "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, phone, active `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&courier.Id,
		&courier.Name,
		&courier.Phone,
		&courier.Active,
	); err != nil {
		u.log.Error("error updating courier in postgres", logger.Error(err))
	}

	return &courier, nil
}

func (u *courierRepo) Delete(ctx context.Context, reuqest *pbp.PrimaryKeyCourier) (*emptypb.Empty, error) {
	query := ` update couriers set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := u.db.Exec(ctx, query, reuqest.GetId())
	return nil, err
}
