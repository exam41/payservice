package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"

	pbp "payservice/genproto/payment_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type staffsRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewStaffRepo(db *pgxpool.Pool, logger logger.ILogger) storage.IStaffsStorage {
	return &staffsRepo{
		db:  db,
		log: logger,
	}
}

func (u *staffsRepo) Create(ctx context.Context, request *pbp.CreateStaff) (*pbp.Staffs, error) {
	staffs := pbp.Staffs{}

	query := `insert into staffs (id, first_name, last_name, phone, login, password, sales_point_id,user_role,searching_column) values ($1, $2, $3, $4, $5, $6, $7, $8,$9) returning id, first_name, last_name, phone, login, password, sales_point_id, user_role, searching_column `
	searchingColumn := fmt.Sprintf("%s %s %s %s ", request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetSalesPointId())

	if err := u.db.QueryRow(ctx, query, uuid.New().String(), request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetLogin(), request.GetPassword(), request.GetSalesPointId(), request.GetUserRole(), searchingColumn).Scan(
		&staffs.Id,
		&staffs.FirstName,
		&staffs.LastName,
		&staffs.Phone,
		&staffs.Login,
		&staffs.Password,
		&staffs.SalesPointId,
		&staffs.UserRole,
	); err != nil {
		//u.log.Error("error while creating staff in postgres", logger.Error(err))
		fmt.Println("Err", err)
	}
	return &staffs, nil
}

func (u *staffsRepo) Get(ctx context.Context, request *pbp.PrimaryKeyStaffs) (*pbp.Staffs, error) {

	staffs := pbp.Staffs{}

	query := ` select id, first_name, last_name, phone, login, password, sales_point_id,user_role from staffs where deleted_at=0 and id=$1 `

	var firstname, last_name, sales_point_id, phone sql.NullString
	if err := u.db.QueryRow(ctx, query, request.GetId()).Scan(
		&staffs.Id,
		&staffs.FirstName,
		&staffs.LastName,
		&staffs.Phone,
		&staffs.Login,
		&staffs.Password,
		&staffs.SalesPointId,
		&staffs.UserRole,
	); err != nil {
		//u.log.Error("error while getting staff in postgres", logger.Error(err))
		fmt.Println("err", err)
	}
	if firstname.Valid {
		staffs.FirstName = firstname.String
	}

	if last_name.Valid {
		staffs.LastName = last_name.String
	}

	if sales_point_id.Valid {
		staffs.SalesPointId = sales_point_id.String
	}

	if phone.Valid {
		staffs.Phone = phone.String
	}

	return &staffs, nil
}

func (u *staffsRepo) GetList(ctx context.Context, request *pbp.StaffRequest) (*pbp.StaffResponse, error) {

	var (
		resp   = pbp.StaffResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}

	countQuery := ` select count(*) from staffs ` + filter

	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("Error counting staffs", logger.Error(err))
	}

	query := ` select id, first_name, last_name, phone, login, password, sales_point_id,user_role  from staffs ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := u.db.Query(ctx, query)

	if err != nil {
		u.log.Error("error while getting staffs in postgres ", logger.Error(err))
	}

	defer rows.Close()

	var firstname, last_name, sales_point_id, phone sql.NullString
	for rows.Next() {
		staff := pbp.Staffs{}
		if err := rows.Scan(&staff.Id, &staff.FirstName, &staff.LastName, &staff.Phone, &staff.Login, &staff.Password, &staff.SalesPointId, &staff.UserRole); err != nil {
			u.log.Error("error while getting staffs in postgres ", logger.Error(err))

		}

		if firstname.Valid {
			staff.FirstName = firstname.String
		}

		if last_name.Valid {
			staff.LastName = last_name.String
		}

		if sales_point_id.Valid {
			staff.SalesPointId = sales_point_id.String
		}

		if phone.Valid {
			staff.Phone = phone.String
		}

		resp.Staffs = append(resp.Staffs, &staff)
	}

	resp.Count = count
	return &resp, nil

}

func (u *staffsRepo) Update(ctx context.Context, request *pbp.Staffs) (*pbp.Staffs, error) {

	var (
		staffs = pbp.Staffs{}
		params = make(map[string]interface{})
		query  = ` update staffs set  `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("staff id", request.GetId())
	if request.GetFirstName() != "" {
		params["first_name"] = request.FirstName
		filter += " first_name= @first_name, "
	}
	if request.GetLastName() != "" {
		params["last_name"] = request.LastName
		filter += " last_name= @last_name, "
	}
	if request.GetPhone() != "" {
		params["phone"] = request.Phone
		filter += " phone= @phone, "
	}
	if request.GetLogin() != "" {
		params["login"] = request.Login
		filter += " login= @login, "
	}
	if request.GetPassword() != "" {
		params["password"] = request.Password
		filter += " password= <PASSWORD>, "
	}
	if request.GetSalesPointId() != "" {
		params["sales_point_id"] = request.SalesPointId
		filter += " sales_point_id= @sales_point_id, "
	}
	if request.GetUserRole() != "" {
		params["user_role"] = request.UserRole
		filter += " user_role= @user_role, "
	}

	query += filter + ` updated_at=now() where deleted_at=0 returning id, first_name, last_name, phone, login, password, sales_point_id, user_role `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := u.db.QueryRow(ctx, fullQuery, args...).Scan(
		&staffs.Id,
		&staffs.FirstName,
		&staffs.LastName,
		&staffs.Phone,
		&staffs.Login,
		&staffs.Password,
		&staffs.SalesPointId,
		&staffs.UserRole,
	); err != nil {
		u.log.Error("error while updating staff in postgres", logger.Error(err))
	}
	return &staffs, nil

}

func (u *staffsRepo) Delete(ctx context.Context, request *pbp.PrimaryKeyStaffs) (*emptypb.Empty, error) {

	query := ` update staffs set deleted_at =extract(epoch from current_timestamp) where id=$1`

	if _, err := u.db.Exec(ctx, query, request.GetId()); err != nil {
		u.log.Error("error while deleting staff in postgres", logger.Error(err))
	}
	return &emptypb.Empty{}, nil
}
