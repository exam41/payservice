package postgres

import (
	"context"
	"errors"
	"fmt"
	"payservice/pkg/helper"
	"payservice/pkg/logger"
	"payservice/storage"
	"time"

	pbp "payservice/genproto/payment_service"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type saleRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSaleRepo(db *pgxpool.Pool, log logger.ILogger) storage.ISalesStorage {
	return &saleRepo{
		db:  db,
		log: log,
	}
}
func (s *saleRepo) Create(ctx context.Context, request *pbp.Sales) (*pbp.Sales, error) {

	sale := pbp.Sales{}
	query := `insert into sales(id,barcode,shift_id,sale_point_id,staff_id,status,searching_column) values($1,$2,$3,$4,$5,$6,$7)
	returning id,barcode,shift_id,sale_point_id,staff_id,status`

	searchingColumn := fmt.Sprintf("%s %s %s %s %s %s", request.GetBarcode(), request.GetShiftId(), request.GetSalePointId(), request.GetStaffId(), request.GetStatus(), request.GetShiftId())

	if err := s.db.QueryRow(ctx, query, request.Id, request.GetBarcode(), request.GetShiftId(), request.GetSalePointId(), request.GetStaffId(), request.GetStatus(), searchingColumn).Scan(
		&sale.Id,
		&sale.Barcode,
		&sale.ShiftId,
		&sale.SalePointId,
		&sale.StaffId,
		&sale.Status,
	); err != nil {
		//	s.log.Error("error while creating sales in postgres", logger.Error(err))
		fmt.Println("error while creating sales in postgres", err)
	}
	return &sale, nil
}

func (s *saleRepo) Get(ctx context.Context, request *pbp.PrimaryKeySale) (*pbp.Sales, error) {
	sales := pbp.Sales{}

	query := `select id,barcode,shift_id,sale_point_id,staff_id,status from sales where deleted_at=0 and id=$1`

	if err := s.db.QueryRow(ctx, query, request.Id).Scan(
		&sales.Id,
		&sales.Barcode,
		&sales.ShiftId,
		&sales.SalePointId,
		&sales.StaffId,
		&sales.Status,
	); err != nil {
		//s.log.Error("error while getting sales in postgres", logger.Error(err))
		fmt.Println("Err", err)
	}
	return &sales, nil
}

func (s *saleRepo) GetList(ctx context.Context, request *pbp.SaleRequest) (*pbp.SalesResponse, error) {
	var (
		resp   = pbp.SalesResponse{}
		filter = " where deleted_at=0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)
	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := ` select count(*) from sales ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error counting sales", logger.Error(err))
	}
	query := ` select id,barcode,shift_id,sale_point_id,staff_id,status from sales ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := s.db.Query(ctx, query)
	if err != nil {
		s.log.Error("error while getting sales in postgres", logger.Error(err))
	}
	defer rows.Close()
	for rows.Next() {
		sales := pbp.Sales{}
		if err := rows.Scan(
			&sales.Id,
			&sales.Barcode,
			&sales.ShiftId,
			&sales.SalePointId,
			&sales.StaffId,
			&sales.Status,
		); err != nil {
			s.log.Error("error while getting sales in postgres", logger.Error(err))
		}
		resp.Sales = append(resp.Sales, &sales)
	}
	resp.Count = count
	return &resp, nil
}

func (s *saleRepo) Update(ctx context.Context, request *pbp.Sales) (*pbp.Sales, error) {
	var (
		sales  = pbp.Sales{}
		params = make(map[string]interface{})
		query  = ` update sales set  `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("sales point id", request.GetId())
	if request.GetBarcode() != "" {
		params["barcode"] = request.Barcode
		filter += " barcode= @barcode, "
	}
	if request.GetShiftId() != "" {
		params["shift_id"] = request.ShiftId
		filter += " shift_id= @shift_id, "
	}

	if request.GetSalePointId() != "" {
		params["sale_point_id"] = request.SalePointId
		filter += " sale_point_id= @sale_point_id, "
	}
	if request.GetStaffId() != "" {
		params["staff_id"] = request.StaffId
		filter += " staff_id= @staff_id, "
	}
	if request.GetStatus() != "" {
		params["status"] = request.Status
		filter += " status= @status, "
	}

	query += filter + ` updated_at=now() where deleted_at=0 and id=@id returning id,barcode,shift_id,sale_point_id,staff_id,status `

	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&sales.Id,
		&sales.Barcode,
		&sales.ShiftId,
		&sales.SalePointId,
		&sales.StaffId,
		&sales.Status,
	); err != nil {
		s.log.Error("error while updating sales in postgres", logger.Error(err))
	}
	return &sales, nil
}

func (s *saleRepo) Delete(ctx context.Context, request *pbp.PrimaryKeySale) (*emptypb.Empty, error) {
	query := ` update sales set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := s.db.Exec(ctx, query, request.Id)
	return nil, err
}

func (s *saleRepo) CountQuery(ctx context.Context, request *pbp.Date) (*pbp.CountQueryy, error) {
	var countt int32
	var id string
	counquery := pbp.CountQueryy{}
	querycount := `SELECT COUNT(*) FROM sales WHERE deleted_at = 0 AND created_at = $1`
	err := s.db.QueryRow(ctx, querycount, request.Date).Scan(&countt)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			id = time.Now().Format("2006-01-02") + "1"
			countt = 0
		} else {
			fmt.Println("Error is here", err)
		}
	}

	counquery.Id = id
	counquery.Count = countt
	return &counquery, nil
}
func (s *saleRepo) CountSale(ctx context.Context, request *emptypb.Empty) (*pbp.PrimaryKeySale, error) {

	count := pbp.PrimaryKeySale{}
	query := ` SELECT id FROM sales ORDER BY id DESC LIMIT 1 `

	var id string
	err := s.db.QueryRow(ctx, query).Scan(&id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			fmt.Println("dd")
			id = "S-0001"
		} else {
			return nil, err
		}
	}
	count.Id = id
	return &count, nil

}
