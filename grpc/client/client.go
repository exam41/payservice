package client

import (
	"payservice/config"
	"payservice/genproto/payment_service"
	repo "payservice/genproto/repo_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	BranchService() payment_service.BranchesServiceClient
	CourierService() payment_service.CourierServiceClient
	SaleService() payment_service.SalesServiceClient
	SalePointService() payment_service.SalesPointsServiceClient
	SaleProductService() payment_service.SaleProductsServiceClient
	ShiftService() payment_service.ShiftsServiceClient
	StaffService() payment_service.StaffsServiceClient
	Income() repo.IncomeProductServiceClient
	Storage() repo.StorageServiceClient
}

type grpcClients struct {
	branchService      payment_service.BranchesServiceClient
	courierService     payment_service.CourierServiceClient
	saleService        payment_service.SalesServiceClient
	salePointService   payment_service.SalesPointsServiceClient
	shiftService       payment_service.ShiftsServiceClient
	staffService       payment_service.StaffsServiceClient
	saleProductService payment_service.SaleProductsServiceClient
	income             repo.IncomeProductServiceClient
	storage            repo.StorageServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connPaymentService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connReposervice,err:=grpc.Dial(
		cfg.REPO_GRPC_HOST+cfg.REPO_GRPC_PORT,
		grpc.WithInsecure(),
	)
	if err!= nil {
        return nil, err
    }
	return &grpcClients{
		branchService:      payment_service.NewBranchesServiceClient(connPaymentService),
		courierService:     payment_service.NewCourierServiceClient(connPaymentService),
		saleService:        payment_service.NewSalesServiceClient(connPaymentService),
		shiftService:       payment_service.NewShiftsServiceClient(connPaymentService),
		staffService:       payment_service.NewStaffsServiceClient(connPaymentService),
		salePointService:   payment_service.NewSalesPointsServiceClient(connPaymentService),
		saleProductService: payment_service.NewSaleProductsServiceClient(connReposervice),
		income:             repo.NewIncomeProductServiceClient(connReposervice),
	}, nil
}

func (g *grpcClients) BranchService() payment_service.BranchesServiceClient {
	return g.branchService
}

func (g *grpcClients) CourierService() payment_service.CourierServiceClient {
	return g.courierService
}
func (g *grpcClients) SaleService() payment_service.SalesServiceClient {
	return g.saleService
}
func (g *grpcClients) SalePointService() payment_service.SalesPointsServiceClient {
	return g.salePointService
}
func (g *grpcClients) SaleProductService() payment_service.SaleProductsServiceClient {
	return g.saleProductService
}
func (g *grpcClients) ShiftService() payment_service.ShiftsServiceClient {
	return g.shiftService
}
func (g *grpcClients) StaffService() payment_service.StaffsServiceClient {
	return g.staffService
}
func (g *grpcClients) Income() repo.IncomeProductServiceClient {
	return g.income
}
func (g *grpcClients) Storage() repo.StorageServiceClient  {
	return g.storage
}