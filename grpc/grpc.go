package grpc

import (
	pbp "payservice/genproto/payment_service"
	"payservice/grpc/client"
	"payservice/pkg/logger"
	"payservice/service"
	"payservice/storage"

	"google.golang.org/grpc"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbp.RegisterBranchesServiceServer(grpcServer, service.NewBranchService(strg,services,log))
	pbp.RegisterCourierServiceServer(grpcServer, service.NewCourierService(strg,services,log))
	pbp.RegisterSalesServiceServer(grpcServer, service.NewSaleService(strg,services,log))
	pbp.RegisterShiftsServiceServer(grpcServer, service.NewShiftService(strg,services,log))
	pbp.RegisterStaffsServiceServer(grpcServer, service.NewStaffService(strg,services,log))
	pbp.RegisterSalesPointsServiceServer(grpcServer, service.NewSalePointService(strg,services,log))
	pbp.RegisterSaleProductsServiceServer(grpcServer, service.NewSaleProductService(strg,services,log))
	return grpcServer
}
