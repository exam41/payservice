package genid

import (
	"fmt"
	"strconv"
)

func GenerateID(lastID string) string {
	lastNumStr := lastID[2:] 
	lastNum, _ := strconv.Atoi(lastNumStr)
	nextNum := lastNum + 1
	nextID := fmt.Sprintf("Sh-%04d", nextNum) 
	return nextID
}
