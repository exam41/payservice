package generate

import (
	"fmt"
	"strconv"
)

func GenerateNextID(lastID, name string) string {
	n := name[:1]
	lastNumStr := lastID[2:]
	lastNum, _ := strconv.Atoi(lastNumStr)
	nextNum := lastNum + 1
	nextID := fmt.Sprintf("%s-%04d", n, nextNum)
	return nextID
}
